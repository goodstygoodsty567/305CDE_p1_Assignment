

exports.register =  async function (user_name, password, email,verify_code, connection, transporter) {
    //let ids = object.ids;
    let returnResult = 0;
    let insertResult = await new Promise(function(resolve, reject) {
        resolve(insertDataForRegister(user_name, password, email,verify_code, connection));
    });
    let sendEmailResuslt = -1;
    if(insertResult == 1){
        sendEmailResuslt = await new Promise(function(resolve,reject){
            resolve(sendEmail(transporter, user_name,email, verify_code));
        });
    }else{
        returnResult = 0;
    }

    if(sendEmailResuslt == 1){
        returnResult = 1;
    }else{
        returnResult = 0;
    }
    console.log("this is the final result"+ returnResult);
    return returnResult;
};


function sendEmail(transporter, user_name,email, verify_code){
    return new Promise(function (resolve,reject) {

        //let hrefLink = "http://52.78.70.14:8080/emailVer?code="+verify_code;
        let hrefLink = "http://52.78.70.14:8080/emailVer/"+'{"code":"'+verify_code+'"}';
        let mailOptions = {
            from: 'angulargoodsty@gmail.com',
            to: email,
            subject: 'Registration Vertification!',
            html: '<h1>Thank You For Register!!</h1><br/><h2>Please copy the Link To Finish The Registration!</h2>' +
            '<br/><h2>User Name:'+user_name+'</h2><p>'+hrefLink+'</p>'
        };

        transporter.sendMail(mailOptions, function(error, info){
            if (!error) {
                console.log(error);
                console.log("This is ok for send Email");
                resolve(1);
            } else {
                console.log(error);
                console.log("this is error for send email");
                console.log('Email sent: ' + info.response);
                resolve(0);
            }
        });
    });
}



function insertDataForRegister(user_name, password, email,verify_code, connection){
    let status = "N";
    let query = 'INSERT INTO `mydb`.`user` (`user_name`, `password`, `email`, `status`,`verify_code`) VALUES ("'+user_name+'", "'+password+'", "'+email+'", "'+status+'","'+verify_code+'");';

    return new Promise(function(resolve, reject){
        connection.query(query, function(err, rows, fields) {
            if (!err){

                console.log("this is inserted for register");
                resolve(1);

            } else{

                console.log("this is error for register");
                console.log(err);
                resolve(0);

            }
        });
    });
}
