

exports.searchBook =  async function (userInput,connection) {
    //let ids = object.ids;

    let searchByBook = await new Promise(function(resolve, reject) {
        resolve(searchByBookName(userInput,connection));
    });

    let searchByAuthorResult = await new Promise(function(resolve,reject){
        resolve(searchByAuthor(userInput,connection));
    });

    let returnResult = [searchByBook,searchByAuthorResult];
    console.log(returnResult);
    return returnResult;
};



function searchByBookName(user_input, connection){
    let query = 'SELECT * FROM mydb.new_view where title = "'+user_input+'";';

    return new Promise(function(resolve, reject){
        connection.query(query, function(err, rows, fields) {
            if (!err){
                resolve(rows);
            } else{
                resolve([]);
            }
        });
    });
}

function searchByAuthor(user_input, connection){
    let query = 'SELECT * FROM mydb.new_view where authors = "'+user_input+'";';

    return new Promise(function(resolve, reject){
        connection.query(query, function(err, rows, fields) {
            if (!err){
                resolve(rows);
            }else{
                resolve([]);
            }
        });
    });
}
