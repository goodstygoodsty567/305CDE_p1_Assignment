

exports.addToWishList =  async function(bookId,comment,userID,connection) {
    let returnResult = -1;

    let isExistResult = await new Promise(function(resolve, reject) {
        resolve(isListExist(userID,connection));
    });

    console.log("is Exist:"+isExistResult);

    if(isExistResult == 0) {
        let createListResult = await new Promise(function (resolve, reject) {
            resolve(createList(userID,connection));
        });

        if (createListResult == 1) {
            let listId = await new Promise(function (resolve, reject) {
                resolve(selectListID(userID, connection));
            });
            if (listId != -1) {
                let addToListResult = await new Promise(function (resolve, reject) {
                    resolve(addToList(bookId, comment, userID, listId, connection));
                });

                if (addToListResult == 1) {
                    returnResult = 1;
                } else {
                    returnResult = 0;
                }

            } else {
                returnResult = 0;
            }


        } else {
            returnResult = 0;
        }
    }else{
        let listId = await new Promise(function (resolve, reject) {
            resolve(selectListID(userID, connection));
        });
        if (listId != -1) {
            let addToListResult = await new Promise(function (resolve, reject) {
                resolve(addToList(bookId, comment, userID, listId, connection));
            });

            if (addToListResult == 1) {
                returnResult = 1;
            } else {
                returnResult = 0;
            }

        } else {
            returnResult = 0;
        }
    }

    return returnResult;
};




function selectListID(userID,connection){
    let query = 'SELECT favourite_id FROM mydb.favourite_list where user_user_id = "'+userID+'";';

    return new Promise(function(resolve, reject){
        connection.query(query, function(err, rows, fields) {
            if (!err){
                resolve(rows[0].favourite_id);
            } else{
                console.log("this is the error while selecting the favorite list id");
                resolve(-1);
            }
        });
    });
}

function addToList(bookId,comment,userID,listId,connection){
    let query = 'INSERT INTO `mydb`.`favourite_item` (`book_id_fk`, `favourite_list_id`, `comment`) VALUES ("'+bookId+'", "'+listId+'", "'+comment+'");';
    console.log(query);
    return new Promise(function(resolve, reject){
        connection.query(query, function(err, rows, fields) {
            if (!err){
                resolve(1);
            } else{
                console.log(err);
                console.log("this is the error while addToList the favorite list id");
                resolve(0);
            }
        });
    });
}





//INSERT INTO `mydb`.`favourite_list` (`user_user_id`) VALUES ('111');

function createList(userID,connection){
    let query = 'INSERT INTO `mydb`.`favourite_list` (`user_user_id`, `identifier`) VALUES ("'+userID+'", "'+"null"+'");';

    return new Promise(function(resolve, reject){
        connection.query(query, function(err, rows, fields) {
            if (!err){
                resolve(1);
            } else{
                console.log(err);
                console.log("error while create list1");
                resolve(0);
            }
        });
    });
}


function isListExist(userID,connection){
    let query = 'SELECT * FROM mydb.favourite_list where user_user_id = "'+userID+'";';
    console.log(query);
    return new Promise(function(resolve, reject){
        connection.query(query, function(err, rows, fields) {
            if (!err){
                console.log(rows);
                console.log(rows.length);
                if(rows.length > 0 ){

                    resolve(1);
                }else{
                    resolve(0);
                }
            } else{
                console.log(err);
                console.log("error while create list2");
                resolve(0);
            }
        });
    });
}




