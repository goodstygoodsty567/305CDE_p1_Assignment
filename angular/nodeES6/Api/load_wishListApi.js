/**
 * Created by goodsty on 11/5/2017.
 */


exports.loadWishList =  async function (userName,connection) {
    //let ids = object.ids;
    let returnResult = [];
    let selectUserIDResult = await new Promise(function(resolve, reject) {
        resolve(selectUserID(userName,connection));
    });

    if(selectUserIDResult != -10){
        let selectListIDResult = await new Promise(function(resolve,reject){
            resolve(selectListID(selectUserIDResult,connection));
        });

        console.log(selectListIDResult);
        if(selectListIDResult != -10){

            let selectWishList = await new Promise(function(resolve,reject){
                resolve(selectWishListBy(selectListIDResult,connection));
            });

            returnResult = selectWishList;

        }else {
            returnResult = [];
        }
    }else{
        returnResult = [];
    }
    console.log(returnResult);
    return returnResult;
};



function selectWishListBy(listId, connection){
    let query = 'SELECT * FROM mydb.wishListView where favourite_list_id = "'+listId+'";';
    console.log(query);
    return new Promise(function(resolve, reject){
        connection.query(query, function(err, rows, fields) {
            if (!err){
                console.log(rows);
                resolve(rows);
            } else{
                resolve([]);
            }
        });
    });
}

function selectListID(userId, connection){
    let query = 'SELECT * FROM mydb.favourite_list where user_user_id = "'+userId+'";';
    console.log(query);
    return new Promise(function(resolve, reject){
        connection.query(query, function(err, rows, fields) {
            console.log(rows);
            if (!err){
                if(rows.length > 0) {
                    console.log(rows[0].favourite_id);
                    resolve(rows[0].favourite_id);
                }else{
                    resolve(-10);
                }
            }else{
                console.log("this is selectListID err");
                console.log(err);
                resolve(-10);
            }
        });
    });
}


function selectUserID(userName, connection){
    let query = 'SELECT * FROM mydb.user where user_name ="'+userName+'" ;';
    console.log(query);
    return new Promise(function(resolve, reject){
        connection.query(query, function(err, rows, fields) {
            if (!err){
                console.log(rows);
                console.log(rows[0]);
                resolve(rows[0].user_id);
            }else{
                console.log("this is selectUserID err");
                console.log(err);
                resolve(-10);
            }
        });
    });
}
