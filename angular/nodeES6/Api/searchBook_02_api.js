


let insert_book_api = require('./insert_book_api.js');


exports.searchBook02 =  async function (userInput,connection,request) {
    //let ids = object.ids;

    let searchByBook = await new Promise(function(resolve, reject) {
        resolve(insertBook(userInput,connection,request));
    });


    //console.log(returnResult);
    return searchByBook;
};



function isBookExist(id, connection){
    let query = 'SELECT * FROM mydb.new_view where book_ori_id ="'+id+'";';

    return new Promise(function(resolve, reject){
        connection.query(query, function(err, rows, fields) {
            if (!err){
                console.log("this is ok on isBookExist");
                if(rows.length>0){
                    resolve(1);
                }else{
                    resolve(0)
                }
            }else{
                console.log("this is error on isBookExist");
                console.log(err);
                resolve(0);
            }
        });
    });
}


function selectBookById(id, connection){
    let query = 'SELECT * FROM mydb.new_view where book_ori_id ="'+id+'";';

    return new Promise(function(resolve, reject){
        connection.query(query, function(err, rows, fields) {
            if (!err){
                console.log("this is ok on selectBookById");
                resolve(rows[0]);
            }else{
                console.log("this is error on selectBookById");
                console.log(err);
                resolve([]);
            }
        });
    });
}







async function insertBook(name,connection,request){
    return new Promise(function(resolve2, reject){
        request('https://www.googleapis.com/books/v1/volumes?q='+name, function (error, response, body) {
            //console.log('error:', error); // Print the error if one occurred
            //console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            if(!error && body.length >30 ){

                let jonData = JSON.parse(body);
                let itemsArray = jonData.items;
                //console.log(itemsArray[0]);
                let data = itemsArray[0];

                //  (type,title, authors, publisher, publishedDate, description, pageCount, averageRating,
                //      ratingsCount, imageLinks, previewLink, infoLink, webReaderLink, country, language, book_ori_id,connection)
                let type = data.volumeInfo.categories;
                if(!type){ type = "null"}
                let title = data.volumeInfo.title;
                if(!title){ title = "null"}

                let authors = "";
                let authorsarray = data.volumeInfo.authors;
                if(!authorsarray){
                    authors = "null"
                }else{
                    authors = data.volumeInfo.authors[0];
                }








                let publisher = data.volumeInfo.publisher;
                if(!publisher){ publisher = "null"}
                let publishedDate = data.volumeInfo.publishedDate;
                if(!publishedDate){ publishedDate = "null"}
                let description = data.volumeInfo.description;
                if(!description){ description = "null"}
                let pageCount = data.volumeInfo.pageCount;
                if(!pageCount){ pageCount = "null"}
                let averageRating = data.volumeInfo.averageRating;
                if(!averageRating){
                    averageRating = 0;
                }
                //console.log(averageRating);
                let ratingsCount = data.volumeInfo.ratingsCount;
                if(!ratingsCount){
                    ratingsCount = 0;
                }
                //console.log(ratingsCount);
                let imageLinks = data.volumeInfo.imageLinks.thumbnail;
                if(!imageLinks){ imageLinks = "null"}
                let previewLink = data.volumeInfo.previewLink;
                if(!previewLink){ previewLink = "null"}
                let infoLink = data.volumeInfo.infoLink;
                if(!infoLink){ infoLink = "null"}
                let webReaderLink = "null";
                let country = "null";
                let language = data.volumeInfo.language;
                if(!language){ language = "null"}
                let book_ori_id = data.id;
                if(!book_ori_id){ book_ori_id = "null"}

                let sepcialID = data.id;

                // insert_book_api.insert_book(type,title, authors, publisher, publishedDate, description, pageCount, averageRating, ratingsCount,
                //     imageLinks, previewLink, infoLink, webReaderLink, country, language, book_ori_id,connection);

                let isExistSelectResult = new Promise(function(resolve, reject) {
                    resolve(isBookExist(sepcialID,connection));
                });

                isExistSelectResult.then(function (value) {
                    if(value == 0){
                        let selectCatIDResult = new Promise(function (resolve,reject) {
                            resolve(insert_book_api.insert_book(type,title, authors, publisher, publishedDate, description.replace(/[\\$'"]/g, "\\$&"), pageCount, averageRating, ratingsCount,
                                imageLinks, previewLink, infoLink, webReaderLink, country, language, book_ori_id,connection));
                        });

                        selectCatIDResult.then(function (value) {

                            let finalSelectResult = new Promise(function (resolve,reject) {
                                resolve(selectBookById(sepcialID,connection));
                            });

                            finalSelectResult.then(function (value) {
                                resolve2(value);
                            });

                        }, function (value) {

                            resolve2(1);
                        });
                    }else{
                        let finalSelectResult = new Promise(function (resolve,reject) {
                            resolve(selectBookById(sepcialID,connection));
                        });

                        finalSelectResult.then(function (value) {
                            resolve2(value);
                        });
                    }
                });

            }

        });
    });

}



