

exports.insert_book =  async function(type,title, authors, publisher, publishedDate, description, pageCount, averageRating, ratingsCount, imageLinks, previewLink, infoLink, webReaderLink, country, language, book_ori_id,connection)  {
    //let ids = object.ids;
    let returnResult = null;

    let selectCatIDResult = await new Promise(function (resolve,reject) {
       resolve(selectCatId(type,connection));
    });

    if(selectCatIDResult != -1 && selectCatIDResult !=-2){
        console.log("in the insertBOOK");
        let insertBookResult = await new Promise(function(resolve, reject) {
            resolve(insertBook(title, authors, publisher, publishedDate, description, pageCount, averageRating, ratingsCount, imageLinks, previewLink, infoLink, webReaderLink, country, language, selectCatIDResult, book_ori_id,connection));
        });

        if(insertBookResult == 1){
            returnResult = 1;
        }else{
            returnResult = 0;
        }
    }else{
        let insertCatResult = await new Promise(function(resolve, reject) {
            resolve(insertCat(type,connection));
        });
        if(insertCatResult == 1){
            let selectCatIDForInsertBook = await new Promise(function(resolve, reject) {
                resolve(selectCatId(type,connection));
            });
            let newInsertBookResult = -10;
            if(selectCatIDForInsertBook != -1 && selectCatIDForInsertBook !=-2){
                newInsertBookResult = await new Promise(function(resolve, reject) {
                    resolve(insertBook(title, authors, publisher, publishedDate, description, pageCount, averageRating, ratingsCount, imageLinks, previewLink, infoLink, webReaderLink, country, language, selectCatIDForInsertBook, book_ori_id,connection));
                });
            }

            if(newInsertBookResult == 1){
                returnResult = 1;
            }else{
                returnResult = 0;
            }
        }
    }



    console.log(returnResult);
    return returnResult;
};

//(title, authors, publisher, publishedDate, description, pageCount, averageRating, ratingsCount, imageLinks, previewLink, infoLink, webReaderLink, country, language, category_category_id, book_ori_id)

function insertBook(title, authors, publisher, publishedDate, description, pageCount, averageRating, ratingsCount, imageLinks, previewLink, infoLink, webReaderLink, country, language, category_category_id, book_ori_id,connection){
    let query = 'INSERT INTO `mydb`.`book` (`title`, `authors`, `publisher`, `publishedDate`, `description`, `pageCount`, `averageRating`, `ratingsCount`, `imageLinks`, `previewLink`, `infoLink`, `webReaderLink`, `country`, `language`, `category_category_id`, `book_ori_id`) VALUES' +
        ' ("'+title+'", "'+authors+'", "'+publisher+'", "'+publishedDate+'", "'+description+'", "'+pageCount+'", "'+averageRating+'", "'+ratingsCount+'", "'+imageLinks+'", "'+previewLink+'", "'+infoLink+'", "'+webReaderLink+'", "'+country+'", "'+language+'", "'+category_category_id+'", "'+book_ori_id+'");';
    console.log(query);
    return new Promise(function(resolve, reject){
        connection.query(query, function(err, rows, fields) {
            if (!err){
                console.log("this is ok on insertBook");

                resolve(1);
            } else{
                console.log("this is error on insertBook");
                console.log(err);
                resolve(0);
            }
        });
    });
}

function selectCatId(type,connection){
    let query = 'SELECT category_id FROM mydb.category where category ="'+type+'";';
    console.log(query);
    return new Promise(function(resolve, reject){
        connection.query(query, function(err, rows, fields) {
            if (!err){
                console.log("this is ok on selectCatId");
                if(rows.length>0){
                    console.log(rows);
                    console.log(rows[0].category_id);
                    resolve(rows[0].category_id);
                }else{
                    console.log("this is no data");
                    resolve(-1);
                }

            } else{
                console.log("this is error on selectCatId");
                console.log(err);
                resolve(-2);
            }
        });
    });
}

function insertCat(type,connection) {
    let query ='INSERT INTO `mydb`.`category` (`category`) VALUES ("'+type+'");';
    return new Promise(function(resolve, reject){
        connection.query(query, function(err, rows, fields) {
            if (!err){
                console.log("this is ok on insertCat");
                resolve(1)

            } else{
                console.log("this is error on insertCat");
                console.log(err);
                resolve(0);
            }
        });
    });
}



