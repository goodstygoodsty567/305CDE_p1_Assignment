
src="entity/User.js";


var User_Module = (function () {

	return {
		create_user: function(user_name, name, email, password, age){
			var new_user = new User();
			new_user.id = random_unique_id();
		 	new_user.user_name = user_name;
		 	new_user.name = name;
		 	new_user.email = email;
		 	new_user.password = password;
		 	new_user.age = age;
		 	return new_user;
		}
	};
	
}());



function random_unique_id()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 7; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}