


var List_Module = (function () {

	return {
		gen_list: function(user_list_array){

			document.getElementById('user_table').innerHTML ='';
            var table_data = '<table><tr><th>ID</th><th>Name</th><th>User Name</th><th>Email</th><th>Age</th><th>Password</th></tr>';
            var table_row = '';
            for(var i = 0 ; i < user_list_array.length ; i++){

                var new_table_td = '';
                new_table_td = new_table_td + '<tr>'; 
                new_table_td = new_table_td + '<td>'+ user_list_array[i].id + '</td>';
                new_table_td = new_table_td + '<td>'+ user_list_array[i].name + '</td>';
                new_table_td = new_table_td + '<td>'+ user_list_array[i].user_name + '</td>';
                new_table_td = new_table_td + '<td>'+ user_list_array[i].email + '</td>';
                new_table_td = new_table_td + '<td>'+ user_list_array[i].age + '</td>';
                new_table_td = new_table_td + '<td>'+ user_list_array[i].password + '</td>';
                new_table_td = new_table_td + '</tr>'; 
                table_row = table_row + new_table_td;
            }
            document.getElementById('user_table').innerHTML = table_data + table_row +'</table>' ;
		},

		average_age: function(array_for_average_age){
			var sum_of_age = 0;
            var result = 0 ; 
            for(var i = 0 ; i < user_array.length ; i++){
                sum_of_age = sum_of_age + Number(user_array[i].age) ;
            }
            result = Math.round(Number(sum_of_age)/Number((user_array.length)));
            alert("The average age of users is " + result );
		},

		sort_list_by_name: function(user_array_list){

			var name_array = [];
			var or_name_array = [];
			for(var j = 0 ; j < user_array_list.length ; j++){
			 	name_array[j] = user_array_list[j].name;
			 	or_name_array[j] = user_array_list[j].name;
			}
			var sorted_name = name_array.sort();
			var count = 0;

			for(var g = 0 ; g < user_array_list.length ;  g++ ){
				if(new String(sorted_name[g]).valueOf() == new String(or_name_array[g]).valueOf()){
					count = count + 1;
				}
			}
			console.log(count);

			if(count == user_array_list.length){
				sorted_name = sorted_name.reverse();
			}

			var store_all_map = {};
			for(var z = 0 ; z < user_array_list.length ; z++){
				store_all_map[user_array_list[z].name] = user_array_list[z];
			}


			var result_array = [];
			for(var k = 0 ; k < user_array_list.length ; k++){
				result_array[k] = store_all_map[sorted_name[k]];
				user_array[k] = store_all_map[sorted_name[k]];
			}
				
			List_Module.gen_list(user_array);
			
		},

		sort_list_by_age: function(user_array_list_age){
			if(isSortedAce == false){
				user_array_list_age.sort(function(a, b) {
    				return Number(a.age) - Number(b.age);
				});
				List_Module.gen_list(user_array_list_age);
				isSortedAce = true;
			}else{
				user_array_list_age.sort(function(a, b) {
    				return Number(b.age) - Number(a.age);
				});
				List_Module.gen_list(user_array_list_age);
				isSortedAce = false;
			}
		},

		sort_age_by_greater_or_equal:function(user_array,number){
			var result_array = [];

			for(var i = 0 ; i < user_array.length ; i++){
				if(Number(user_array[i].age) >= Number(number)){
					result_array.push(user_array[i]);
				}
			}

			List_Module.gen_list(result_array);

		},

		sort_age_by_lesster_or_equal:function(user_array,number){
			var result_array = [];

			for(var i = 0 ; i < user_array.length ; i++){
				if(Number(user_array[i].age) <= Number(number)){
					result_array.push(user_array[i]);
				}
			}
			List_Module.gen_list(result_array);

		}



	};
	
}());