


var validationMessageController = (function () {

	return {
		show_valid_message_by_id: function(id,input_id){
			document.getElementById(input_id).style.border = '1.5px solid green';
			document.getElementById(id).style.display = 'none';
		},
		show_invalid_message_by_id: function(id,input_id) {
			document.getElementById(input_id).style.border = '1.5px solid red';
			document.getElementById(id).style.display = 'block';
			document.getElementById(id).innerHTML = "The email format is invalid.";
		},
		show_empty_message:function(id,input_id){
			document.getElementById(input_id).style.border = '1.5px solid red';
			document.getElementById(id).style.display = 'block';
			document.getElementById(id).innerHTML = "The field is required.";
		}
		,
		reset_form:function(){
            document.getElementById('username_id').value = "";
            document.getElementById('name_id').value= ""; 
            document.getElementById('email_id').value= "";
            document.getElementById('password_id').value= "";
            document.getElementById('age_id').value= "";
            document.getElementById('username_id').style.border = '1px solid #dbdbdb';
			document.getElementById('name_wrong_message').style.display = 'none';
			document.getElementById('name_id').style.border = '1px solid #dbdbdb';
			document.getElementById('name_wrong_message').style.display = 'none';
			document.getElementById('email_id').style.border = '1px solid #dbdbdb';
			document.getElementById('email_message').style.display = 'none';
			document.getElementById('password_id').style.border = '1px solid #dbdbdb';
			document.getElementById('password_message').style.display = 'none';
			document.getElementById('age_id').style.border = '1px solid #dbdbdb';
			document.getElementById('age_message').style.display = 'none';
			usernamecounted = false;
    		namecounted = false;
    		passwordcounted = false;
    		agecounted = false;
    		emailcounted = false;
		}
	};
	
}());

var validationChecker = (function () {
	
	return {
		check_user_name: function(user_name,id,input_id){
			if(user_name.length){
				var count_same = 0;
				for(var i = 0 ; i < user_array.length ; i++){
					if(new String(user_array[i].user_name).valueOf() == new String(user_name).valueOf() ){
						count_same = count_same +1;
					}
				}
				if(count_same > 0){
					usernamecounted = false;
					document.getElementById(input_id).style.border = '1.5px solid red';
					document.getElementById(id).style.display = 'block';
					document.getElementById(id).innerHTML = "The user name has been used.";
				}else{
					validationMessageController.show_valid_message_by_id(id,input_id);
					usernamecounted = true;
				}
			}else{
				usernamecounted = false;
				validationMessageController.show_empty_message(id,input_id);
			}
		},
		check_name: function(name,id,input_id) {
			if(name){
				validationMessageController.show_valid_message_by_id(id,input_id);
				namecounted = true;
				
			}else{
				validationMessageController.show_empty_message(id,input_id);
				namecounted = false;
			}
		},
		check_email: function(email,id,input_id) {
			if(email){
				if(validEmail(email)){
					validationMessageController.show_valid_message_by_id(id,input_id);
					emailcounted = true;
				}else{
					validationMessageController.show_invalid_message_by_id(id,input_id);
					emailcounted = false;
				}
			}else{
				validationMessageController.show_empty_message(id,input_id);
				emailcounted = false;
			}
		}
		,
		check_password: function(password, id, input_id) {
			if(password){
				if(password.length < 6){
					document.getElementById(input_id).style.border = '1.5px solid red';
					document.getElementById(id).style.display = 'block';
					document.getElementById(id).innerHTML = "The length of password requires at least 6 digits";
					passwordcounted = false;
				}else{
					validationMessageController.show_valid_message_by_id(id,input_id);
					passwordcounted = true;
				}
			}else{
				validationMessageController.show_empty_message(id,input_id);
				passwordcounted = false;
			}
		}
		,
		chck_age: function(age, id, input_id) {
			if(age){
				validationMessageController.show_valid_message_by_id(id,input_id);
				agecounted = true;
				
			}else{
				validationMessageController.show_empty_message(id,input_id);
				agecounted = false;
			}
		}
		
	};
	
}());


function validEmail(email){
      if(!email){
        return false;
      }
      var r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?");
      return ((email).match(r) == null) ? false : true;
}







